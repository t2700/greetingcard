import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import ballon from "./extraImg/ballon.jpg";
import bird from "./extraImg/bird.jpg";
import bird1 from "./extraImg/bird1.jpg";
import hands from "./extraImg/hands.jpg";
import puzzle from "./extraImg/puzzle.jpg";
import { v4 as uuidv4 } from "uuid";

import ImageUploading from "react-images-uploading";
function ExtraImg({
  addStatus,
  setAddStatus,
  extraImgState,
  setExtraImgState,
  extraImg,
  setExtraImg,
}) {
  const [gallary, setGallary] = useState([]);
  const [images, setImages] = React.useState([]);
  const maxNumber = 69;
  const { v4: uuidv4 } = require("uuid");

  const onChange = (imageList, addUpdateIndex) => {
    // data for submit
    console.log(imageList, addUpdateIndex);
    setImages(imageList);
  };
  let imgGall = [
    { value: ballon },
    { value: bird },
    { value: bird1 },
    { value: hands },
    { value: puzzle },
  ];

  useEffect(() => {
    // debugger;
    setGallary(imgGall);
  });

  const style = {
    container1: {
      position: "fixed",
      background: "gray",
      height: "100vh",
      width: "100vw",
      zIndex: 1,
    },
    container2: {
      position: "relative",
      height: "100vh",
      width: "70vw",
      background: "white",
      overflowY: "scroll",
    },
    timesicon: {
      position: "absolute",
      top: "0",
      right: "5px",
      fontSize: "25px",
    },
    image: {
      width: "30vh",
      height: "30vh",
    },
    container3: {
      marginTop: "70px",
      overflowY: "scroll",
      height: "50vh",
      width: "50vw",
      marginLeft: "10vh",
    },
  };
  const closewindow = () => {
    setExtraImg(false);
  };
  return (
    <Grid container style={style.container1}>
      <Grid container style={style.container2}>
        <i
          class="fas fa-times"
          style={style.timesicon}
          onClick={closewindow}
        ></i>
        <Grid container>
          <Grid container style={style.container3}>
            {imgGall.map((val) => {
              //    debugger;
              return (
                <Grid item xs={3}>
                  <img
                    src={val.value}
                    style={style.image}
                    onClick={(e) => {
                      debugger;
                      setExtraImgState({
                        ...extraImgState,
                        key: uuidv4(),
                        top: "0",
                        left: "0",
                        value: val.value,
                        outline: false,
                        transform: "0",
                        height: 20,
                        width: 20,
                      });
                      setExtraImg(false);
                      setAddStatus(false);
                    }}
                  />
                </Grid>
              );
            })}
          </Grid>
          <Grid container>
            <Grid>
              <h2 style={{ marginLeft: "40vh" }}> Add a Custom File </h2>
              <ImageUploading
                // multiple
                value={images}
                onChange={onChange}
                maxNumber={maxNumber}
                dataURLKey="data_url"
              >
                {({
                  imageList,
                  onImageUpload,
                  onImageRemoveAll,
                  onImageUpdate,
                  onImageRemove,
                  isDragging,
                  dragProps,
                }) => (
                  // write your building UI
                  <div className="upload__image-wrapper">
                    <i
                      class="fas fa-upload"
                      onClick={onImageUpload}
                      style={{
                        fontSize: "50px",
                        paddding: "10px",
                        marginLeft: "50vh",
                      }}
                    >
                      {" "}
                    </i>
                    &nbsp;
                    {imageList.map((image, index) => (
                      <div
                        key={index}
                        className="image-item"
                        style={{ marginLeft: "47vh" }}
                      >
                        <img src={image["data_url"]} alt="" width="100" />
                        <div className="image-item__btn-wrapper">
                          <button
                            onClick={() => {
                              debugger;
                              setExtraImgState({
                                ...extraImgState,
                                key: uuidv4(),
                                top: "0",
                                left: "0",
                                value: image["data_url"],
                                outline: false,
                                transform: "0",
                                height: 20,
                                width: 20,
                              });
                              // setExtraImgState(image["data_url"]);
                              setAddStatus(false);
                              setExtraImg(false);
                            }}
                          >
                            Update
                          </button>
                          <button onClick={() => onImageRemove(index)}>
                            Remove
                          </button>
                        </div>
                      </div>
                    ))}
                  </div>
                )}
              </ImageUploading>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default ExtraImg;
