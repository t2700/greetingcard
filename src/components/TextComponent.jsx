import { Grid } from "@material-ui/core";
import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";

function TextComponent({
  textStatus,
  setTextStatus,
  textData,
  setTextData,
  userDetails,
  setUserDetails,
  activeId,
  setActiveId,
}) {
  const [nameErr, setNameErr] = useState(false);
  const { v4: uuidv4 } = require("uuid");

  const style = {
    container1: {
      position: "fixed",
      background: "gray",
      height: "100vh",
      width: "100vw",
      zIndex: 1,
    },
    container2: {
      position: "relative",
      height: "20vh",
      width: "70vw",
      margin: "5%",
      background: "white",
    },
    timesicon: {
      position: "absolute",
      top: "0",
      right: "5px",
      fontSize: "20px",
    },
  };
  const close = () => {
    setTextStatus(false);
  };
  const onsubmit = () => {
    debugger;
    console.log(userDetails);
    if (userDetails.value === "") {
      setNameErr(true);
    } else {
      setTextData([...textData, userDetails]);
      setTextStatus(false);
      onClear();
    }
  };
  const onClear = () => {
    setNameErr(false);
    setUserDetails({
      key: "",
      top: "",
      left: "",
      value: "",
      outline: false,
      transform: "",
    });
  };
  const onUpdate = () => {
    debugger;
    console.log(activeId);
    console.log(textData);
    console.log(userDetails);
    let copyData = JSON.parse(JSON.stringify(textData));
    for (let e of copyData) {
      if (e.key === activeId) {
        e.value = userDetails.value;
      }
    }
    setTextData(copyData);
    setTextStatus(false);
    setActiveId("");
    onClear();
  };
  return (
    <Grid container style={style.container1}>
      <Grid container style={style.container2}>
        <i class="fas fa-times" style={style.timesicon} onClick={close}></i>
        <Grid container style={{ marginTop: "10px" }}>
          <Grid container>
            <Grid Grid item xs={12}>
              <input
                style={{
                  width: "69vw",
                  height: "5vh",
                  border: "none",
                  fontSize: "15px",
                }}
                type="text"
                class="form-control"
                id="inputName"
                placeholder="Text"
                value={userDetails.value}
                onChange={(e) =>
                  setUserDetails({
                    ...userDetails,
                    key: uuidv4(),
                    top: "0",
                    left: "0",
                    value: e.target.value,
                    outline: false,
                    transform: "0",
                    fontFamily: "",
                    fontSize: "",
                    color: "",
                  })
                }
              />
              {nameErr && (
                <p style={{ color: "red", fontSize: "13px" }}>
                  Please Enter Text
                </p>
              )}
              {activeId ? (
                <button onClick={onUpdate}>Update</button>
              ) : (
                <button onClick={onsubmit}>Submit</button>
              )}
              <button onClick={onClear}>Clear</button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default TextComponent;
