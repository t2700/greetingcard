import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import image1 from "./assets/image1.jpg";
import image2 from "./assets/image2.png";
import image3 from "./assets/image3.jpg";
import image4 from "./assets/image4.jpg";
import image5 from "./assets/image5.jpg";
import image6 from "./assets/image6.jpg";
import image7 from "./assets/image7.png";
import image8 from "./assets/image8.jpg";
import image9 from "./assets/image9.png";
import image10 from "./assets/image10.jpg";
import image11 from "./assets/image11.jpg";
import image12 from "./assets/image12.png";
import image13 from "./assets/image13.jpg";
import image14 from "./assets/image14.png";
import image15 from "./assets/image15.jpg";
import ImageUploading from "react-images-uploading";
function AddPhotos({
  photoStatus,
  setPhotoStatus,
  imageStatu,
  setImageStatu,
  addStatus,
  setAddStatus,
}) {
  const [gallary, setGallary] = useState([]);
  const [images, setImages] = React.useState([]);
  const maxNumber = 69;

  const onChange = (imageList, addUpdateIndex) => {
    // data for submit
    console.log(imageList, addUpdateIndex);
    setImages(imageList);
  };
  let imgGall = [
    image1,
    image2,
    image3,
    image4,
    image5,
    image6,
    image7,
    image8,
    image9,
    image10,
    image11,
    image12,
    image13,
    image14,
    image15,
  ];
  useEffect(() => {
    // debugger;
    setGallary(imgGall);
  });
  useEffect(() => {
    console.log(photoStatus);
  }, [photoStatus]);
  const style = {
    container1: {
      position: "fixed",
      background: "gray",
      height: "100vh",
      width: "100vw",
      zIndex: 1,
    },
    container2: {
      position: "relative",
      height: "100vh",
      width: "70vw",
      background: "white",
      overflowY: "scroll",
    },
    timesicon: {
      position: "absolute",
      top: "0",
      right: "5px",
      fontSize: "25px",
    },
    image: {
      width: "30vh",
      height: "30vh",
    },
    container3: {
      marginTop: "70px",
      overflowY: "scroll",
      height: "50vh",
      width: "50vw",
      marginLeft: "10vh",
    },
  };
  const closewindow = () => {
    setPhotoStatus(false);
  };
  const change = () => {};
  return (
    <Grid container style={style.container1}>
      <Grid container style={style.container2}>
        <i
          class="fas fa-times"
          style={style.timesicon}
          onClick={closewindow}
        ></i>
        <Grid container>
          <Grid container style={style.container3}>
            {imgGall.map((val) => {
              // debugger;
              return (
                <Grid item xs={3}>
                  <img
                    src={val}
                    style={style.image}
                    onClick={() => {
                      setImageStatu(val);
                      setPhotoStatus(false);
                      setAddStatus(false);
                    }}
                  />
                </Grid>
              );
            })}
          </Grid>
          <Grid container>
            <Grid>
              <h2 style={{ marginLeft: "40vh" }}> Add a Custom File </h2>
              <ImageUploading
                // multiple
                value={images}
                onChange={onChange}
                maxNumber={maxNumber}
                dataURLKey="data_url"
              >
                {({
                  imageList,
                  onImageUpload,
                  onImageRemoveAll,
                  onImageUpdate,
                  onImageRemove,
                  isDragging,
                  dragProps,
                }) => (
                  // write your building UI
                  <div className="upload__image-wrapper">
                    <i
                      class="fas fa-upload"
                      onClick={onImageUpload}
                      style={{
                        fontSize: "50px",
                        paddding: "10px",
                        marginLeft: "50vh",
                      }}
                    >
                      {" "}
                    </i>
                    &nbsp;
                    {imageList.map((image, index) => (
                      <div
                        key={index}
                        className="image-item"
                        style={{ marginLeft: "47vh" }}
                      >
                        <img src={image["data_url"]} alt="" width="100" />
                        <div className="image-item__btn-wrapper">
                          <button
                            onClick={() => {
                              setImageStatu(image["data_url"]);
                              setAddStatus(false);
                              setPhotoStatus(false);
                            }}
                          >
                            Update
                          </button>
                          <button onClick={() => onImageRemove(index)}>
                            Remove
                          </button>
                        </div>
                      </div>
                    ))}
                  </div>
                )}
              </ImageUploading>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default AddPhotos;
