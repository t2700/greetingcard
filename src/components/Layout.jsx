import React, { useState, useEffect, createRef } from "react";
import Grid from "@material-ui/core/Grid";
import AddPhotos from "./AddPhotos";
import image3 from "./assets/image3.jpg";
import image1 from "./assets/image1.jpg";

import TextComponent from "./TextComponent";
import Draggable from "react-draggable"; // The default
import { useScreenshot, createFileName } from "use-react-screenshot";
import { ColorPicker, useColor } from "react-color-palette";
import "react-color-palette/lib/css/styles.css";
import TransRotateCom from "react-drag-rotate";
import ExtraImg from "./ExtraImg";

function Layout({
  photoStatus,
  setPhotoStatus,
  textStatus,
  setTextStatus,
  extraImg,
  setExtraImg,
}) {
  const [addStatus, setAddStatus] = useState(false);
  const [imageStatu, setImageStatu] = useState(image3);
  const [textData, setTextData] = useState([]);
  const [imageData, setImageData] = useState([]);
  const [enableText, setEnableText] = useState(false);
  const [activeId, setActiveId] = useState("");
  const [imageactiveId, setImageActiveId] = useState("");
  const [enableFontStyle, setEnableFontStyle] = useState(false);
  const [enableFontSize, setEnableFontSize] = useState(false);
  const [enableFontColor, setEnableFontColor] = useState(false);
  const [userDetails, setUserDetails] = useState({});
  const ref = createRef(null);
  const dragref = createRef();
  const [extraImgState, setExtraImgState] = useState({});
  const [extraImageOnClick, setExtraImageOnClick] = useState(false);
  const [color, setColor] = useColor("hex", "#121212");
  const [image, takeScreenShot] = useScreenshot({
    type: "image/jpeg",
    quality: 1.0,
  });
  const imgRef = createRef();
  const ExtraimgRef = createRef();
  const [imgzoom, setImgZoom] = useState({ height: "100%", width: "100%" });
  const [extraimgzoom, setExtrsImgZoom] = useState({
    height: "10%",
    width: "10%",
  });
  const download = (image, { name = "img", extension = "jpg" } = {}) => {
    const a = document.createElement("a");
    a.href = image;
    a.download = createFileName(extension, name);
    a.click();
  };

  const downloadScreenshot = () => takeScreenShot(ref.current).then(download);

  useEffect(() => {
    debugger;
    if (Object.keys(extraImgState).length !== 0) {
      setImageData([...imageData, extraImgState]);
    }
  }, [extraImgState]);
  const style = {
    icons: {
      fontSize: "30px",
      margin: "10px",
      padding: "10px",
    },
    image: {
      width: imgzoom.width,
      height: imgzoom.height,
      position: "relative",
    },
    text1: {
      position: "absolute",
      top: "0",
      left: "0",
      width: "100px",
      height: "50px",
      border: "3px solid #73AD21",
    },
  };
  const imageReset = () => {
    setImgZoom({ height: "100%", width: "100%" });
  };
  const imageZoomIn = () => {
    debugger;
    const height = imgRef.current.clientHeight;
    const width = imgRef.current.clientWidth;
    setImgZoom({ height: height + 10, width: width + 10 });
  };

  const imageZoomOut = () => {
    debugger;
    const height = imgRef.current.clientHeight;
    const width = imgRef.current.clientWidth;
    setImgZoom({ height: height - 10, width: width - 10 });
  };
  const extraimageZoomIn = () => {
    let copyData = JSON.parse(JSON.stringify(imageData));
    debugger;
    for (let ele of copyData) {
      if (ele.key === imageactiveId) {
        ele.height = ele.height + 10;
        ele.width = ele.width + 10;
      }
    }

    setImageData(copyData);
  };
  const extraimageZoomOut = () => {
    let copyData = JSON.parse(JSON.stringify(imageData));
    debugger;
    for (let ele of copyData) {
      if (ele.key === imageactiveId) {
        ele.height = ele.height - 10;
        ele.width = ele.width - 10;
      }
    }

    setImageData(copyData);
  };

  const addData = () => {
    setAddStatus(!addStatus);
    setEnableFontStyle(false);
    setEnableFontSize(false);
    setEnableFontColor(false);
    setExtraImageOnClick(false);
  };
  const doubleClick = (val) => {
    // debugger
    setEnableText(true);
    setActiveId(val);
  };
  const clickOne = () => {
    debugger;

    let copyData = JSON.parse(JSON.stringify(textData));

    for (let val of copyData) {
      val.outline = false;
    }
    let dataimg = JSON.parse(JSON.stringify(imageData));
    for (let val of dataimg) {
      val.outline = false;
    }
    setTextData(copyData);
    setImageData(dataimg);
    setActiveId("");
    setEnableFontColor(false);
    setEnableFontSize(false);
    setEnableFontStyle(false);
    setEnableText(false);
    setAddStatus(false);
  };
  const ImagesingleClick = (id) => {
    debugger;
    setImageActiveId(id);
    let copyData = JSON.parse(JSON.stringify(imageData));
    for (let ele of copyData) {
      if (ele.key === id) {
        ele.outline = true;
      }
    }
    setImageData(copyData);
  };
  const singleClick = (id) => {
    // debugger
    console.log(textData);
    setActiveId(id);

    let copyData = JSON.parse(JSON.stringify(textData));
    for (let ele of copyData) {
      if (ele.key === id) {
        ele.outline = true;
      }
    }
    setTextData(copyData);
  };
  const clowiseClick = () => {
    debugger;
    console.log(activeId);
    console.log(textData);
    let cData = JSON.parse(JSON.stringify(textData));
    for (let ele of cData) {
      if (activeId === ele.key) ele.transform = parseInt(ele.transform - 10);
    }
    setTextData(cData);
  };
  const anticlockClick = () => {
    debugger;
    console.log(textData);
    let cData = JSON.parse(JSON.stringify(textData));
    for (let ele of cData) {
      if (activeId === ele.key) ele.transform = parseInt(ele.transform + 10);
    }
    setTextData(cData);
  };
  const deleteData = () => {
    debugger;
    let id = activeId;
    let copydata = JSON.parse(JSON.stringify(textData));
    for (let ele in copydata) {
      if (activeId === copydata[ele].key) {
        copydata.splice(ele, 1);
      }
    }
    setTextData(copydata);
    setActiveId("");
  };
  const editData = () => {
    // debugger
    let name;
    console.log(activeId);
    console.log(textData);
    let newdata = textData;
    for (let e of newdata) {
      if (e.key === activeId) {
        name = e;
        break;
      }
    }
    setUserDetails(name);
    setTextStatus(true);
    setEnableText(false);
  };
  const handleDown = (e, id) => {
    debugger;
    setActiveId(id);
    if (e.keyCode === 38) {
      console.log("up");
      console.log(textData);
      let cData = JSON.parse(JSON.stringify(textData));
      for (let ele of cData) {
        if (activeId === ele.key)
          ele.top = String(parseInt(ele.top) - 4) + "px";
      }
      setTextData(cData);
    }
    if (e.keyCode === 37) {
      console.log("left");
      console.log(textData);
      let cData = JSON.parse(JSON.stringify(textData));
      for (let ele of cData) {
        if (activeId === ele.key) {
          debugger;
          ele.left = String(parseInt(ele.left) - 4) + "px";
        }
      }
      setTextData(cData);
    }
    if (e.keyCode === 39) {
      console.log("right");
      console.log(textData);
      let cData = JSON.parse(JSON.stringify(textData));
      for (let ele of cData) {
        if (activeId === ele.key) {
          debugger;
          ele.left = String(parseInt(ele.left) + 4) + "px";
        }
      }
      setTextData(cData);
    }
    if (e.keyCode === 40) {
      console.log("down");
      console.log(textData);
      let cData = JSON.parse(JSON.stringify(textData));
      for (let ele of cData) {
        if (activeId === ele.key) {
          debugger;
          ele.top = String(parseInt(ele.top) + 4) + "px";
        }
      }
      setTextData(cData);
    }
  };
  const textColor = (e) => {
    debugger;
    setColor(e);
    debugger;
    let cData = JSON.parse(JSON.stringify(textData));
    for (let ele of cData) {
      if (activeId === ele.key) {
        debugger;

        ele.colorHEX = e.hex;
      }
    }
    setTextData(cData);
  };

  let fontSize = [
    2, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38,
    40, 42, 44,
  ];
  const fontSizeMethod = (e) => {
    console.log(e.target.id);
    let id = e.target.id;
    console.log(fontSize);

    let cData = JSON.parse(JSON.stringify(textData));
    for (let ele of cData) {
      if (activeId === ele.key) {
        debugger;
        for (let val in fontSize) {
          console.log(val);
          if (val === id) {
            ele.fontSize = fontSize[val] + "px";
          }
        }
      }
    }
    setTextData(cData);
  };

  let fontStyle = [
    "'Water Brush', cursive",
    "Bebas Neue', cursive",
    "'Cairo', sans-serif",
    "'Dancing Script', cursive",
    "'Inconsolata', monospace",
    "'Inter', sans-serif",
    "'Montserrat', sans-serif",
    " 'Ms Madi', cursive",
    "'Orelega One', cursive",
    "'Pacifico', cursive",
    "'PT Sans Narrow', sans-serif",
    "'PT Serif', serif",
    "'Roboto', sans-serif",
    "'Updock', cursive",
    "'My Soul', cursive",
    "'Roboto Mono', monospace",
    "'Slabo 27px', serif",
  ];

  const fontMethod = (e) => {
    debugger;
    console.log(e.target.id);
    let id = e.target.id;
    console.log(fontStyle);

    let cData = JSON.parse(JSON.stringify(textData));
    for (let ele of cData) {
      if (activeId === ele.key) {
        debugger;
        for (let val in fontStyle) {
          console.log(val);
          if (val === id) {
            ele.fontFamily = fontStyle[val];
          }
        }
      }
    }
    setTextData(cData);
  };
  var initX, initY, mousePressX, mousePressY;

  const dragStart = (event) => {
    debugger;
    let ele = document.getElementById(event.target.id);
    if (event.target.id === activeId) {
      initX = ele.offsetLeft;
      initY = ele.offsetTop;
      mousePressX = event.clientX;
      mousePressY = event.clientY;
    }
  };
  const dragEnd = (e) => {
    debugger;
    let ele = document.getElementById(e.target.id);
    ele.style.left = initX + e.clientX - mousePressX + "px";
    ele.style.top = initY + e.clientY - mousePressY + "px";
  };
  const ImagedragStart = (event) => {
    debugger;
    let ele = document.getElementById(event.target.id);
    if (event.target.id === imageactiveId) {
      initX = ele.offsetLeft;
      initY = ele.offsetTop;
      mousePressX = event.clientX;
      mousePressY = event.clientY;
    }
  };
  const ImagedragEnd = (e) => {
    debugger;
    let ele = document.getElementById(e.target.id);
    ele.style.left = initX + e.clientX - mousePressX + "px";
    ele.style.top = initY + e.clientY - mousePressY + "px";
  };
  const handleImage = () => {
    debugger;
    setExtraImageOnClick(!extraImageOnClick);
    setEnableFontStyle(false);
    setEnableFontSize(false);
    setEnableFontColor(false);
    setAddStatus(false);
  };
  return (
    <Grid container>
      {photoStatus && (
        <AddPhotos
          photoStatus={photoStatus}
          setPhotoStatus={setPhotoStatus}
          imageStatu={imageStatu}
          setImageStatu={setImageStatu}
          addStatus={addStatus}
          setAddStatus={setAddStatus}
        />
      )}
      {textStatus && (
        <TextComponent
          textStatus={textStatus}
          setTextStatus={setTextStatus}
          textData={textData}
          setTextData={setTextData}
          activeId={activeId}
          setActiveId={setActiveId}
          userDetails={userDetails}
          setUserDetails={setUserDetails}
        />
      )}
      {extraImg && (
        <ExtraImg
          addStatus={addStatus}
          setAddStatus={setAddStatus}
          extraImgState={extraImgState}
          setExtraImgState={setExtraImgState}
          extraImg={extraImg}
          setExtraImg={setExtraImg}
        />
      )}

      <Grid container style={{ height: "15vh", marginLeft: "90vh" }}>
        <h1>welcome</h1>
      </Grid>
      <Grid container style={{ backgroundColor: "gray" }}>
        <Grid>
          <i class="fas fa-plus" style={style.icons} onClick={addData}></i>
        </Grid>
        <Grid>
          <i class="fas fa-undo" style={style.icons} onClick={clowiseClick}></i>
        </Grid>
        <Grid>
          <i
            class="fas fa-redo"
            style={style.icons}
            onClick={anticlockClick}
          ></i>
        </Grid>
        <Grid>
          <i
            class="fas fa-download"
            style={style.icons}
            onClick={downloadScreenshot}
          ></i>
        </Grid>
        <Grid>
          <i
            class="fas fa-font"
            style={style.icons}
            onClick={() => {
              setEnableFontStyle(!enableFontStyle);
              setEnableFontSize(false);
              setEnableFontColor(false);
              setAddStatus(false);
              setExtraImageOnClick(false);
            }}
          ></i>
        </Grid>
        <Grid>
          <i
            class="fas fa-text-size"
            style={style.icons}
            onClick={() => {
              setEnableFontSize(!enableFontSize);
              setEnableFontStyle(false);
              setEnableFontColor(false);
              setAddStatus(false);
              setExtraImageOnClick(false);
            }}
          ></i>
        </Grid>
        <Grid>
          <i
            class="fas fa-broom"
            style={style.icons}
            onClick={() => {
              setEnableFontColor(!enableFontColor);
              setEnableFontStyle(false);
              setEnableFontSize(false);
              setAddStatus(false);
              setExtraImageOnClick(false);
            }}
          ></i>
        </Grid>
        <Grid>
          <i
            class="fas fa-search-plus"
            style={style.icons}
            onClick={imageZoomIn}
          ></i>
        </Grid>
        <Grid>
          <i
            class="fas fa-search-minus"
            style={style.icons}
            onClick={imageZoomOut}
          ></i>
        </Grid>
        <Grid>
          <i class="fas fa-sync" style={style.icons} onClick={imageReset}></i>
        </Grid>
        <Grid>
          <i class="fas fa-image" style={style.icons} onClick={handleImage}></i>
        </Grid>
        <Grid>
          {enableText && (
            <i class="fas fa-pen" style={style.icons} onClick={editData}></i>
          )}
        </Grid>
        <Grid>
          {enableText && (
            <i
              class="fas fa-trash"
              style={style.icons}
              onClick={deleteData}
            ></i>
          )}
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={4} style={{ position: "relative" }}>
          {enableFontColor && (
            <Grid container>
              <ColorPicker
                width={456}
                height={228}
                color={color}
                hideHSV
                dark
                onChange={(e) => textColor(e)}
              />
            </Grid>
          )}
          {enableFontSize && (
            <Grid
              container
              style={{
                width: "15%",
                border: "1px solid black",
                overflow: "auto",
                height: "200px",
              }}
            >
              {fontSize.map((val, index) => {
                console.log(val);
                return (
                  <li
                    id={index}
                    onClick={(e) => {
                      fontSizeMethod(e);
                    }}
                  >
                    {val}
                  </li>
                );
              })}
            </Grid>
          )}
          {enableFontStyle && (
            <Grid
              container
              style={{
                width: "60%",
                border: "1px solid black",
                overflow: "auto",
                height: "200px",
              }}
            >
              {fontStyle.map((val, index) => {
                return (
                  <li
                    id={index}
                    onClick={(e) => {
                      fontMethod(e);
                    }}
                  >
                    {val}
                  </li>
                );
              })}
            </Grid>
          )}
          {extraImageOnClick && (
            <Grid>
              <Grid container>
                <i
                  class="fas fa-plus"
                  style={style.icons}
                  onClick={() => {
                    setExtraImg(true);
                  }}
                ></i>
              </Grid>
              <Grid container>
                <i
                  class="fas fa-search-minus"
                  style={style.icons}
                  onClick={extraimageZoomOut}
                ></i>
              </Grid>
              <Grid container>
                <i
                  class="fas fa-search-plus"
                  style={style.icons}
                  onClick={extraimageZoomIn}
                ></i>
              </Grid>
            </Grid>
          )}
          {addStatus && (
            <Grid>
              <Grid container>
                <i
                  class="far fa-image"
                  style={style.icons}
                  onClick={() => {
                    setPhotoStatus(true);
                  }}
                ></i>
              </Grid>
              <Grid container>
                <i
                  class="fas fa-text"
                  style={style.icons}
                  onClick={() => {
                    setTextStatus(true);
                    setActiveId("");
                  }}
                ></i>
              </Grid>
            </Grid>
          )}
        </Grid>
        <Grid
          ref={ref}
          item
          xs={6}
          style={{
            position: "relative",
            height: "500px",
            width: "700px",
            overflowY: "hidden",
            overflowX: "hidden",
          }}
        >
          <div style={{ height: "500px", width: "710px" }}>
            <img
              src={imageStatu}
              ref={imgRef}
              style={style.image}
              onClick={clickOne}
            />
            {console.log(extraImgState)}
            {console.log(imageData)}
            {imageData.map((val, index) => {
              return (
                <img
                  draggable="true"
                  id={val.key}
                  onClick={() => {
                    // debugger;
                    ImagesingleClick(val.key);
                  }}
                  onDragStart={(e) => {
                    ImagedragStart(e);
                  }}
                  onDragEnd={(e) => ImagedragEnd(e)}
                  ref={ExtraimgRef}
                  src={val.value}
                  style={{
                    top: val["top"],
                    left: val["left"],
                    position: "absolute",
                    outlineStyle: val.outline ? "dotted" : "none",
                    transform: `rotate(${val["transform"]}deg)`,
                    height: val["height"],
                    width: val["width"],
                  }}
                />
              );
            })}

            {textData.map((val) => {
              // debugger
              return (
                <>
                  <p
                    draggable="true"
                    id={val.key}
                    onDragStart={(e) => {
                      dragStart(e);
                    }}
                    onDragEnd={(e) => dragEnd(e)}
                    ref={dragref}
                    onClick={() => {
                      // debugger;
                      singleClick(val.key);
                    }}
                    onDoubleClick={() => doubleClick(val.key)}
                    onKeyDown={(e) => handleDown(e, val.key)}
                    tabIndex="0"
                    style={{
                      top: val["top"],
                      left: val["left"],
                      position: "absolute",
                      outlineStyle: val.outline ? "dotted" : "none",
                      transform: `rotate(${val["transform"]}deg)`,
                      fontFamily: val["fontFamily"],
                      fontSize: val["fontSize"],
                      color: val["colorHEX"],
                    }}
                  >
                    {val.value}
                    {console.log(val.value)}
                  </p>
                </>
              );
            })}
          </div>
        </Grid>
        <Grid item xs={2}></Grid>
      </Grid>
    </Grid>
  );
}

export default Layout;
