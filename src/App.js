import logo from "./logo.svg";
import "./App.css";
import Layout from "./components/Layout";
import React, { useState } from "react";

function App() {
  const [photoStatus, setPhotoStatus] = useState(false);
  const [textStatus, setTextStatus] = useState(false);
  const [extraImg, setExtraImg] = useState(false);

  return (
    <div>
      <Layout
        photoStatus={photoStatus}
        setPhotoStatus={setPhotoStatus}
        extraImg={extraImg}
        setExtraImg={setExtraImg}
        textStatus={textStatus}
        setTextStatus={setTextStatus}
      />
    </div>
  );
}

export default App;
